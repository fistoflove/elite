module.exports = function (grunt) {
  grunt.initConfig({
    sass: {
      dev: {
        files: {
          "static/main.css": "dev/scss/source.scss",
          "static/blog.css": "dev/scss/blog.scss",
          "static/product.css": "dev/scss/product.scss",
          "static/job.css": "dev/scss/job.scss",
        },
      },
    },
    watch: {
      files: ["dev/scss/*", "dev/scss/theme/*"],
      tasks: ["sass"],
    },
  });
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-watch");
};
