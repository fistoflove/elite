<?php

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>
        <div class="btn-container">
            <div class="atc df">
                <div class="qty-c df ai-c jc-c">
                    <span class="ns ta-c tc-p4" onclick="document.querySelector('.input-text.qty').stepDown();syncQty();" class="minus">-</span>
                    <?php
                    do_action( 'woocommerce_before_add_to_cart_quantity' );

                    woocommerce_quantity_input(
                        array(
                            'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                            'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                            'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                        )
                    );
                    do_action( 'woocommerce_after_add_to_cart_quantity' );
                    ?>
                    <span class="ns ta-c tc-p4"onclick="document.querySelector('.input-text.qty').stepUp();syncQty();" class="minus">+</span>
                </div>
                <input class="dn" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>">
                <div class="fg btn-tw">
                    <button type="submit" onclick="product_atc(true, this);"  class="single_add_to_cart_button btn btn-t atc"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
                </div>
            </div>

            <!-- <input type="hidden" name="is_buy_now" id="is_buy_now" value="0" />
            <button type="submit" name="add-to-cart" class="single_add_to_cart_button btn btn-s" id="buy_now_button">Buy Now</button> -->
            <div class="btn-sw df">
                <a data-product="<?= $product->get_id(); ?>" onclick="event.preventDefault();product_atc(false, this);" rel="nofollow" name="add-to-cart" class="ta-c btn btn-s atc" id="bnb" href="/checkout/?add-to-cart=<?= $product->get_id();?>&quantity=1" rel="nofollow">Buy Now</a>
            </div>
        </div>
		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>

<script>
    function syncQty() {
        var bnb = document.getElementById('bnb');
        bnb.href = '/checkout/?add-to-cart=' + bnb.dataset.product + '&quantity=' + document.querySelector('.input-text.qty').value;
    }
</script>