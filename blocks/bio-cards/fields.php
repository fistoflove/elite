<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Cards", "repeater", [
            ["Image", "image"],
            ["Name", "text"],
            ["Position", "text"],
        ]],
    ]
);