<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Button", "clone", "group_62e2d5080c52c"],
        ["Items", "repeater", [
            ['Icon', 'image'],
            ['Label', 'text']
        ]],
    ]
);