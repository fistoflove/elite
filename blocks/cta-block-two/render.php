<?php

use Timber\Timber;
use Timber\Post;
use Mold\Helper\Scss;

include_once("fields.php");

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$context['compiled_css']  = Scss::compile_styles(__DIR__);

$context['local_path'] = "/wp-content/themes/elite/blocks/cta-block-two";

Timber::render( 'template.twig', $context);