<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Logo", "image"],
        ["Content", "wysiwyg"],
        ["Background Image", "image"],
        ["Button", "clone", "group_62e2d5080c52c"],
    ]
);