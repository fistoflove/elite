<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "CTA Block",
    [
        ["Title", "text"],
        ["Bakcground Color", "clone", "group_62e2d4737f194"],
        ["Form", "clone", "group_62e2fb45f0acb"],
    ]
);
