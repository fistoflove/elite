<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Boxes", "repeater", [
            ["Image", "image"],
            ["Link", "link"],
        ]],
    ]
);