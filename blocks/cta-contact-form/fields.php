<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "CTA Block",
    [
        ["Title", "text"],
        ["Content", "wysiwyg"],
        ["Button", "clone", "group_62e2d5080c52c"],
        ["Background Color", "clone", "group_62e2d4737f194"],
    ]
);

$fields->register_tab(
    "Form Content",
    [
        ["Title", "text"],
        ["Address", "wysiwyg"],
        ["Phone", "link"],
        ["Email", "link"],
        ["Background Color", "clone", "group_62e2d4737f194"],
    ]
);

$fields->register_tab(
    "Contact Form",
    [
        ["Form", "clone", "group_62e2fb45f0acb"],
    ]
);