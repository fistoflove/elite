<?php

use Timber\Timber;
use Timber\Post;
use Mold\Helper\Scss;
use Mold\Helper\Fields;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$context['compiled_css']  = Scss::compile_styles(__DIR__);

$context['local_path']  = "/wp-content/themes/elite/blocks/shop-page/";

// $query = new WC_Product_Query();

// $products = $query->get_products();

// $context['products'] = $products;

$context['products'] = wc_get_products( array(
    'orderby'  => 'name',
));

$context['categories'] = get_terms( ['taxonomy' => 'product_cat', 'hide_empty' => true] );

Timber::render( 'template.twig', $context);