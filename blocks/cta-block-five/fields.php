<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content", "wysiwyg"],
        ["Image One", "image"],
        ["Image Two", "image"],
        ["Button", "clone", "group_62e2d5080c52c"],
    ]
);