<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Contact Form",
    [
        ["Form", "clone", "group_62e2fb45f0acb"],
    ]
);