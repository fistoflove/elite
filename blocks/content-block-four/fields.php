<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Text", "wysiwyg"],
        ["Image", "image"],
    ]
);

$fields->register_tab(
    "List",
    [
        ["Items", "repeater", [
            ["Image", "image"],
            ["Title", "text"]
        ]],
    ]
);