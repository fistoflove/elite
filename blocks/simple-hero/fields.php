<?php

use Mold\Helper\Fields;
use Timber\Timber;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Content", "wysiwyg"],
        ["Button", "clone", "group_62e2d5080c52c"]
    ]
);