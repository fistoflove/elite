<?php

use Mold\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content", "wysiwyg"],
        ["Image", "image"],
        ["Button", "clone", "group_62e2d5080c52c"],
        ["Order", "clone", "group_62e2e2efd08a3"],
    ]
);