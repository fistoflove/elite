<?php
use Timber\Timber;

  function acf_filter_rest_api_preload_paths( $preload_paths ) {
    if ( ! get_the_ID() ) {
      return $preload_paths;
    }
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '?context=edit';
    $v1 =  array_filter(
      $preload_paths,
      function( $url ) use ( $remove_path ) {
        return $url !== $remove_path;
      }
    );
    $remove_path = '/wp/v2/' . get_post_type() . 's/' . get_the_ID() . '/autosaves?context=edit';
    return array_filter(
      $v1,
      function( $url ) use ( $remove_path ) {
        return $url !== $remove_path;
      }
    );
  }

  add_filter( 'block_editor_rest_api_preload_paths', 'acf_filter_rest_api_preload_paths', 10, 1 );

   add_filter( 'timber/twig', 'add_to_twig_theme' );

   function add_to_twig_theme( $twig ) {
     $twig->addFunction( new Twig_Function( '_ims_get_product_variations', '_ims_get_product_variations' ) );
     $twig->addFunction( new Twig_Function( '_ims_get_product_image', '_ims_get_product_image' ) );
     $twig->addFunction( new Twig_Function( '_ims_get_product_price', '_ims_get_product_price' ) );
     $twig->addFunction( new Twig_Function( '_ims_get_product_variation_price', '_ims_get_variation_price' ) );
     $twig->addFunction( new Twig_Function( 'ims_get_product_price', 'ims_get_product_price' ) );
     $twig->addFunction( new Twig_Function( '_ims_build_radio_input_value', '_ims_build_radio_input_value' ) );
     $twig->addFunction( new Twig_Function( '_ims_get_product_info_variable', '_ims_get_product_info_variable' ) );
     $twig->addFunction( new Twig_Function( '_ims_get_product_info_simple', '_ims_get_product_info_simple' ) );
     $twig->addFunction( new Twig_Function( 'ims_get_product_variation_data', 'ims_get_product_variation_data' ) );
     $twig->addFunction( new Twig_Function( 'ims_get_cart', 'ims_get_cart' ) );
     $twig->addFunction( new Twig_Function( 'ims_get_slug', 'ims_get_slug' ) );
     $twig->addFunction( new Twig_Function( 'ims_get_currency_code', 'ims_get_currency_code' ) );
     $twig->addFunction( new Twig_Function( 'ims_get_product_primary_category', 'ims_get_product_primary_category' ) );
     return $twig;
   }

   function _ims_get_product_info_simple($post) {
    $product = wc_get_product($post->id);
    $attachment_ids = $product->get_gallery_image_ids();

    $data = [
      "images" => [],
    ];

    array_push($data['images'], get_the_post_thumbnail_url($post->id));

    foreach( $attachment_ids as $attachment_id ) {
        array_push($data['images'], wp_get_attachment_url( $attachment_id ));
    }

    return $data;
   }

   function _ims_get_product_info_variable($post) {

    $product = wc_get_product($post->id);

    $attachment_ids = $product->get_gallery_image_ids();

    $data = [
      "images" => [],
      "variation_images" => []
    ];

    foreach( $attachment_ids as $attachment_id ) {
        array_push($data['images'], wp_get_attachment_url( $attachment_id ));
    }

    $variations = $product->get_available_variations();

    foreach ( $variations as $variation ) {
      array_push($data['variation_images'],  [
        $variation['attributes']['attribute_pa_colour'] => $variation['image']['src']
      ]);
    }
    return $data;
   }

   function _ims_get_product_variations($product_id) {
    if(is_array($product_id)) {
      $product = wc_get_product($product_id[0]);
      return $product->get_available_variations();
    } else {
      $product = wc_get_product($product_id);
      return $product->get_available_variations();
    }
   }

   function _ims_get_product_image($product) {

    return get_the_post_thumbnail_url($product);
   }

   function _ims_get_product_price($_product) {
    $product = wc_get_product($_product->ID);
    return $product->get_price_html();
   }

   function ims_get_product_price() {
    global $product;
    $myprice = wc_price($product->get_price());
    return $myprice;
   }

   function ims_get_product_variation_data($obj) {
    $data = [
      "price" => [
        "lowest" => false,
        "highest" => false,
      ],
      "attributes" => [],
      "attr_values" => [],
    ];

    foreach ($obj as $var) {
      // From To pricing
      if(!$data['price']['lowest'] || $data['price']['lowest'] < $var['display_price'] ) {
        $data['price']['lowest'] = $var['price_html'];
      }
      if(!$data['price']['highest'] || $data['price']['highest'] > $var['display_price'] ) {
        $data['price']['highest'] = $var['price_html'];
      }

      foreach ($var['attributes'] as $k => $v) {
        if(!in_array($k, $data['attributes'])) {
          array_push($data['attributes'], $k);
        }
        $data['attr_values'][$v] = $var['variation_id'];
      }
    }
    return $data;
   }

   function _ims_build_radio_input_value($name, $id) {
    $value = str_replace(' ', '-', $name) . '-' . $id;
    return $value;
   }

   add_action( 'wp_ajax_nopriv_ims_get_builder_results', 'ims_get_builder_results' );

   add_action( 'wp_ajax_ims_ims_get_builder_results', 'ims_get_builder_results' );
   
   function ims_get_builder_results() {
   
    $context = Timber::get_context();

    $context['results'] = "foo";

    $context['foo'] = 'bar';

    Timber::render( 'builder-results.twig', $context );
    die();
   }
 
   add_action( 'wp_ajax_nopriv_ims_get_videos', 'ims_get_videos' );

   add_action( 'wp_ajax_ims_get_videos', 'ims_get_videos' );
   
   
   function ims_get_videos() {
    $builder_options = get_field('builder_options', 'option');
    $unit = $_REQUEST['unit'];
    $length = $_REQUEST['garden-length'];
    $height = $_REQUEST['height-required'];
    $colour = $_REQUEST['colour'];
    $trellis = $_REQUEST['trellis'];
    $plinth = $_REQUEST['plinth'];
    $postcover = $_REQUEST['post-cover'];

    if($unit == "meter") {
      $height = round($_REQUEST['height-required'] * 3.28084, 2);
      $height_display = round($_REQUEST['height-required'], 1);
      $length = $_REQUEST['garden-length'];
    } else {
      $length = round($_REQUEST['garden-length'] * 0.3048);
      $length_display = round($_REQUEST['garden-length'], 2);
      $height = $_REQUEST['height-required'];
    }

    // Calculated Values
    $panels_required = $length / 1.88;

    $rounded = ceil($panels_required);

    if($trellis == "nothanks") {
      $trellis_required = 0;
    } else {
      $trellis_s = explode('-', $trellis);
      $trellis_required = $rounded;
      $trellis_id = array_pop($trellis_s);
      $trellis_name = implode(' ', $trellis_s);

      $trellis_colour_value = explode('-', $_REQUEST['trellis-colour']);

      $trellis_id = array_pop($trellis_colour_value);

      $trellis_colour = implode(' ', $trellis_colour_value);
    }

    if($plinth == "nothanks") {
      $plinth_required = 0;
    } else {
      $plinth_s = explode('-', $plinth);
      $plinth_required = $rounded;
      $plinth_id = array_pop($plinth_s);
      $plinth_name = implode(' ', $plinth_s);

      $plinth_colour_value = explode('-', $_REQUEST['plinth-colour']);

      $plinth_id = array_pop($plinth_colour_value);

      $plinth_colour = implode(' ', $plinth_colour_value);
    }

    if($postcover == "nothanks") {
      $postcover_required = 0;
    } else {
      $postcover_s = explode('-', $postcover);
      $postcover_required = $rounded;
      $postcover_id = array_pop($postcover_s);
      $postcover_name = implode(' ', $postcover_s);

      $postcover_colour_value = explode('-', $_REQUEST['postcover-colour']);

      $postcover_id = array_pop($postcover_colour_value);

      $postcover_colour = implode(' ', $postcover_colour_value);
    }

    $infill_sections = ($height * $rounded) - $trellis_required - $plinth_required;

    $full_packs_required = ceil(($infill_sections / 5));

    $full_packs_required_rounded = ceil($full_packs_required);

    $extra_top_rail = $rounded - $full_packs_required_rounded;

    if($extra_top_rail < 0) {
      $extra_top_rail = 0;
    }

    $concrete_screws_included = $full_packs_required_rounded * 6;

    if($height == 6) {
      $concrete_screws_required = ($full_packs_required_rounded * 8) - $concrete_screws_included;;
    } else {
      $concrete_screws_required = ($full_packs_required_rounded * 6) - $concrete_screws_included;
    }

    $channel_adapters_m = ($rounded * (1.5 * 2)) - ($full_packs_required_rounded * 2 * 1.5) + ($trellis_required * 2 * 0.3);

    $channel_adapters_extra = ceil(($channel_adapters_m / 1.5));

    // if($height == 6) {
    //   $small_adapters = $rounded * 2;
    // } else {
    //   $small_adapters = 0;
    // }

    // if($height == 6) {
    //   $extra_channel_adapters = 0;
    // } else {
    //   $extra_channel_adapters = 0;
    //   $extra_channel_adapters = ceil($channel_adapters_m / 1.5);
    //   if ($small_adapters > 0) {
    //     $extra_channel_adapters = 0;
    //   } else {
    //     $extra_channel_adapters = ceil($channel_adapters_m / 1.5);
    //   }
    // }


    $product = wc_get_product($builder_options['section_two']['smartfence_product'][0]);
    $product_variations = $product->get_available_variations();

    foreach($product_variations as $variation) {
      if($variation['attributes']['attribute_pa_colour'] == $colour) {
        // $variation_image = $variation['image']['url'];
        $variation_price = $variation['display_price'];
        $variation_id = $variation['variation_id'];

        $variation_image = '/wp-content/themes/elite/images/builder/' . $variation['attributes']['attribute_pa_colour'] . '.jpg';
        if($trellis_required > 0) {
          $whichtrellis = str_replace(" ", "-", strtolower($trellis_name));
          $whichtrelliscolor = str_replace(" ", "-", strtolower($trellis_colour));
          $variation_image = '/wp-content/themes/elite/images/builder/' . $variation['attributes']['attribute_pa_colour'] . '-' . $whichtrellis . '-' . $whichtrelliscolor . '.jpg';
          $variation_image = '/wp-content/themes/elite/images/builder/' . $variation['attributes']['attribute_pa_colour'] . '-' . $whichtrellis . '.jpg';
        }
      }

    }

    if($trellis_required > 0) {
      $trellis_product = wc_get_product($trellis_id);
      $trellis_price = $trellis_product->get_price();
    }

    if($plinth_required > 0) {
      $plinth_product = wc_get_product($plinth_id);
      $plinth_price = $plinth_product->get_price();
    }

    if($postcover_required > 0) {
      $postcover_product = wc_get_product($postcover_id);
      $postcover_price = $postcover_product->get_price();
    }

    $screw_product = wc_get_product($builder_options['misc']['screw_product'][0]);

    $screw_variations = $screw_product->get_available_variations();

    foreach($screw_variations as $variation) {
      if($variation['attributes']['attribute_pa_colour'] == $colour) {
        $screw_price = $variation['display_price'];
        $screw_product_id = $variation['variation_id'];
      }
    }

    $top_rail_product = wc_get_product($builder_options['misc']['top_rail_product'][0]);

    $top_rail_variations = $top_rail_product->get_available_variations();

    foreach($top_rail_variations as $variation) {
      if($variation['attributes']['attribute_pa_colour'] == $colour) {
        $top_rail_price = $variation['display_price'];
        $top_rail_id = $variation['variation_id'];
      }
    }

    $std_channel_adapter_product = wc_get_product($builder_options['misc']['standard_channel_adapter_product'][0]);

    $std_channel_adapter_variations = $std_channel_adapter_product->get_available_variations();

    foreach($std_channel_adapter_variations as $variation) {
      if($variation['attributes']['attribute_pa_colour'] == $colour) {
        $std_channel_adapter_price = $variation['display_price'];
        $std_channel_adapter_id = $variation['variation_id'];
      }
    }

    $std_channel_adapters_required = $extra_top_rail * 2;

    $short_channel_adapter_product = wc_get_product($builder_options['misc']['short_channel_adapter_product'][0]);

    $short_channel_adapter_variations = $short_channel_adapter_product->get_available_variations();

    foreach($short_channel_adapter_variations as $variation) {
      if($variation['attributes']['attribute_pa_colour'] == $colour) {
        $short_channel_adapter_price = $variation['display_price'];
        $short_channel_adapter_id = $variation['variation_id'];
      }
    }

    $short_channel_adapters_required = $extra_top_rail * 2;

    $extra_screws = $extra_top_rail;
    if($plinth_required > 0) {
      $short_channel_adapters_required = 0;

    } else {
      if($height == 6) {
        $short_channel_adapters_required = $rounded * 2;
      } else {
        $short_channel_adapters_required = 0;
      }
    }

    $colored_screws_included = $full_packs_required_rounded * 6;

    $colored_screws_required = $colored_screws_included - ($full_packs_required_rounded * 6);




    // echo "Height required: " . $height . "<br>";
    // echo "Panels required: " . $panels_required . "<br>";
    // echo "Rounded: " . $rounded . "<br>";
    // echo "Infill Sections: " . $infill_sections . "<br>";
    // echo "Full packs required: " . $full_packs_required_rounded . "<br>";

    // echo "<h3>Results:</h3><br>";
    // echo "colored screws included " . $colored_screws_included . "<br>";
    // echo "Full packs required (rounded): " . $full_packs_required_rounded . "<br>";
    // echo "Extra Top Rails: " . $extra_top_rail. "<br>";
    // echo "Colored screws already in pack: " . $colored_screws_included. "<br>";

    // // // New
    // echo "Extra Screw Packs: " . $extra_top_rail . "<br>";
    // echo "No of extra Channel Full Length Adapters: " . $extra_top_rail  * 2 . "<br>";
    // echo "Extra 'Small' Channel Adapters for 6' fence: " . $extra_top_rail  * 2 . "<br>";

    // Old
    // echo "Extra concrete screws required: " . $concrete_screws_required . "<br>";
    // echo "Extra colored screws required: " . $colored_screws_required . "<br>";
    // echo "Channel M Adapter: " . $channel_adapters_m . "<br>";
    // echo "Extra Channel M Adapter: " . $extra_channel_adapters . "<br>";
    // echo "Small Adapters: " . $small_adapters . "<br>";

    $number_of_items = $full_packs_required_rounded + $trellis_required + $plinth_required + $postcover_required + $extra_screws + $extra_top_rail + $std_channel_adapters_required + $short_channel_adapters_required;

    // Prices 

    $variation_price = $full_packs_required * $variation_price;

//    echo $variation_price . "<br>";

    $trellis_price = $trellis_required * $trellis_price;

//    echo $trellis_price . "<br>";

    $plinth_price = $plinth_required * $plinth_price;

//    echo $plinth_price . "<br>";

    $postcover_price = $postcover_required * $postcover_price;

//    echo $postcover_price . "<br>";
    
    $screw_price = $extra_screws * $screw_price;

//    echo $extra_screws_price . "<br>";

    $top_rail_price = $extra_top_rail * $top_rail_price;

 //   echo $top_rail_price . "<br>";

    $std_channel_adapter_price_total = $std_channel_adapter_price * $std_channel_adapters_required;

//    echo $std_channel_adapter_price_total . "<br>";

    $short_channel_adapter_price_total = $short_channel_adapter_price * $short_channel_adapters_required;

//    echo $short_channel_adapter_price_total . "<br>";

    $total_price = $std_channel_adapter_price_total + $short_channel_adapter_price_total + $top_rail_price + $screw_price + $postcover_price + $trellis_price  + $plinth_price + $variation_price;

    $cart_data = [
      ["id" => intval($variation_id), "qty" => $full_packs_required_rounded],
    ];

    if($trellis_required > 0) {
      array_push($cart_data, ["id" => intval($trellis_id), "qty" => $trellis_required]);
    }

    if($plinth_required > 0) {
      array_push($cart_data, ["id" => intval($plinth_id), "qty" => $plinth_required]);
    }

    if($postcover_required > 0) {
      array_push($cart_data, ["id" => intval($postcover_id), "qty" => $postcover_required]);
    }

    if($extra_top_rail > 0) {
      array_push($cart_data, ["id" => intval($top_rail_id), "qty" => $extra_top_rail]);
    }

    if($extra_screws > 0) {
      array_push($cart_data, ["id" => intval($screw_product_id), "qty" => $extra_screws]);
    }

    if($std_channel_adapters_required > 0) {
      array_push($cart_data, ["id" => intval($std_channel_adapter_id), "qty" => $std_channel_adapters_required]);
    }

    if($short_channel_adapters_required > 0) {
      array_push($cart_data, ["id" => intval($short_channel_adapter_id), "qty" => $short_channel_adapters_required]);
    }

    $new_data = [
      "dimensions" => ($unit == 'meter') ? ceil($length) . 'm Length X ' . $height_display . 'm Height' : $length_display . 'ft Length X ' . ceil($height) . 'ft Height',
      "colour" => str_replace('-', ' ', $colour),
      "variation_image" => $variation_image,
      "variation_price" => $variation_price,
      "variation_id" => $variation_id,
      "packs" => $full_packs_required_rounded,
      "rails" => $extra_top_rail,
      "extra_screws" => $extra_screws,
      "screw_price" => $screw_price,
      "std_channel_adapter_price" => $std_channel_adapter_price_total,
      "std_channel_adapters_required" => $std_channel_adapters_required,
      "short_channel_adapter_price" => $short_channel_adapter_price_total,
      "short_channel_adapters_required" => $short_channel_adapters_required,
      "small_adapters" => $small_adapters,
      "trellis_required" => $trellis_required,
      "trellis_id" => $trellis_id,
      "trellis_name" => $trellis_name,
      "trellis_colour" => $trellis_colour,
      "trellis_price" => $trellis_price,
      "plinth_required" => $plinth_required,
      "plinth_id" => $plinth_id,
      "plinth_name" => $plinth_name,
      "plinth_colour" => $plinth_colour,
      "plinth_price" => $plinth_price,
      "postcover_required" => $postcover_required,
      "postcover_id" => $postcover_id,
      "postcover_name" => $postcover_name,
      "postcover_colour" => $postcover_colour,
      "postcover_required" => $postcover_required,
      "postcover_price" => $postcover_price,
      "extra_top_rail" => $extra_top_rail,
      "top_rail_price" => $top_rail_price,
      "channel_adapter_price" =>  $channel_adapter_price,
      'total' => $number_of_items,
      "total_price" => $total_price,
      "cart_contents" => $cart_data
    ];

    Timber::render( 'builder-results.twig', $new_data);


    die();
   }

   add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

   add_action( 'wp_ajax_nopriv_ims_get_products_by_category', 'ims_get_products_by_category' );

   add_action( 'wp_ajax_ims_get_products_by_category', 'ims_get_products_by_category' );  
   
   function ims_get_products_by_category() {
     $context = Timber::get_context();
     
     if($_REQUEST['category'] !== "false") {
      $context['products'] = wc_get_products( array(
        'orderby' => 'modified',
        'order' => 'DESC',
        'category' => array($_REQUEST['category']),
      ));
     } else {
      $context['products'] = wc_get_products( array(
        'orderby' => 'modified',
        'order' => 'DESC',
      ));
     }

     
    Timber::render( 'product-card-ajax.twig', $context );

    die();
   }

   add_filter( 'wcml_multi_currency_ajax_actions', 'add_action_to_multi_currency_ajax', 10, 1 );
 
   function add_action_to_multi_currency_ajax( $ajax_actions ) {
    $ajax_actions[] = 'ims_get_videos';
    $ajax_actions[] = 'ims_get_products_by_category';
    $ajax_actions[] = 'ims_get_atc_alert';
    $ajax_actions[] = 'ims_get_cart_ajax';

    return $ajax_actions;
   }

   add_action( 'wp_ajax_nopriv_ims_get_cart_ajax', 'ims_get_cart_ajax' );

   add_action( 'wp_ajax_ims_get_cart_ajax', 'ims_get_cart_ajax' );  

   function ims_get_cart_ajax() {

    $data = [
      "count" => WC()->cart->get_cart_contents_count(),
      "total" => WC()->cart->get_total(),
      "products" => []
    ];

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
      $product_line = [];

      $product = $cart_item['data'];

      $product_id = $cart_item['product_id'];

      $quantity = $cart_item['quantity'];

      $price = WC()->cart->get_product_price( $product );

      $subtotal = WC()->cart->get_product_subtotal( $product, $cart_item['quantity'] );

      $link = $product->get_permalink( $cart_item );

      // Anything related to $product, check $product tutorial

      $attributes = $product->get_attributes();

      $product_line = [
        "id" => $product_id,
        "name" => $product->get_title(),
        "qty" => $quantity,
        "price" => $price,
        "subtotal" => $subtotal,
        "link" => $link,
        "img" => $product->get_image('full'),
        "attrs" => $attributes
      ];
      array_push($data['products'], $product_line);
   }
    Timber::render( 'minicart-ajax.twig', $data);
    die();
   }

   function ims_get_cart() {

    $data = [
      "count" => WC()->cart->get_cart_contents_count(),
      "total" => WC()->cart->get_total(),
      "products" => []
    ];
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
      $product_line = [];

      $product = $cart_item['data'];

      $product_id = $cart_item['product_id'];

      $quantity = $cart_item['quantity'];

      $price = WC()->cart->get_product_price( $product );

      $subtotal = WC()->cart->get_product_subtotal( $product, $cart_item['quantity'] );

      $link = $product->get_permalink( $cart_item );

      $attributes = $product->get_attributes();

      $product_line = [
        "id" => $product_id,
        "name" => $product->get_title(),
        "qty" => $quantity,
        "price" => $price,
        "subtotal" => $subtotal,
        "link" => $link,
        "img" => $product->get_image('full'),
        "attrs" => $attributes
      ];

      array_push($data['products'], $product_line);
   }
    return $data;
   }

   function ims_get_slug($product_obj) {
    $product = wc_get_product($product_obj->ID);
    return $product->slug;
   }

   function ims_get_currency_code() {
    global $woocommerce_wpml;
    $currency = $woocommerce_wpml->multi_currency->get_client_currency();
    if($currency == "GBP") {
      echo '£';
    } elseif($currency == "EUR") {
      echo '€';
    }
   }

   function ims_get_product_primary_category($product) {
    $categories = get_the_terms($product->get_id(),'product_cat');
    return $categories[0]->name;
   }

   // Redirects WooCommerce category pages to the default shop page
   add_action( 'template_redirect', function() {
    if ( is_product_category() ) {
        wp_redirect( wc_get_page_permalink( 'shop' ) );
        exit();
    }
  });

  add_action( 'login_enqueue_scripts', function() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/login-logo.svg);
    height:65px;
    width:320px;
    background-size: 320px 65px;
    background-repeat: no-repeat;
          padding-bottom: 30px;
        }
    </style>
  <?php });

add_action( 'template_redirect', 'bt_remove_woocommerce_styles_scripts', 999 );
/**
 * Remove Woo Styles and Scripts from non-Woo Pages
 * @link https://gist.github.com/DevinWalker/7621777#gistcomment-1980453
 * @since 1.7.0
 */
function bt_remove_woocommerce_styles_scripts() {

        // // Skip Woo Pages
        // if ( is_woocommerce() || is_cart() || is_checkout() || is_account_page() ) {
        //         return;
        // }
        // // Otherwise...
        // remove_action('wp_enqueue_scripts', [WC_Frontend_Scripts::class, 'load_scripts']);
        // remove_action('wp_print_scripts', [WC_Frontend_Scripts::class, 'localize_printed_scripts'], 5);
        // remove_action('wp_print_footer_scripts', [WC_Frontend_Scripts::class, 'localize_printed_scripts'], 5);
}

add_action( 'wp_ajax_nopriv_ims_get_atc_alert', 'ims_get_atc_alert' );

add_action( 'wp_ajax_ims_get_atc_alert', 'ims_get_atc_alert' );  

function ims_get_atc_alert() {
  $product = wc_get_product( $_REQUEST['product_id'] );

  $total = $product->get_regular_price() * $_REQUEST['qty'];

  $data = [
    "price" => $total,
    "currency" => get_woocommerce_currency_symbol(),
    "qty" => $_REQUEST['qty']
  ];

  echo json_encode($data);
  die();
  }

  // Hides paid shipping options when free shipping is available.

  add_filter( 'woocommerce_package_rates', function( $rates, $package ) {
    $all_free_rates = array();
    foreach ( $rates as $rate_id => $rate ) {
       if ( 'free_shipping' === $rate->method_id ) {
          $all_free_rates[ $rate_id ] = $rate;
          break;
       }
    }
    if ( empty( $all_free_rates )) {
       return $rates;
    } else {
       return $all_free_rates;
    }
  }, 9999, 2 );
  // Disables woocommerce's shipping rates cache
  // https://github.com/woocommerce/woocommerce/issues/22100
  add_action( 'after_setup_theme', function() {
    WC_Cache_Helper::get_transient_version( 'shipping', true );
  });
  register_post_type( 'Job', [
    'label'                 => __( 'Job', 'ims_theme' ),
    'description'           => __( 'Jobs', 'ims_theme' ),
    'labels'                => [
      'name'                  => _x( 'Jobs', 'Post Type General Name', 'ims_theme' ),
      'singular_name'         => _x( 'Job', 'Post Type Singular Name', 'ims_theme' ),
      'menu_name'             => __( 'Careers', 'ims_theme' ),
      'name_admin_bar'        => __( 'Job', 'ims_theme' ),
      'archives'              => __( 'Job Archives', 'ims_theme' ),
      'attributes'            => __( 'Job Attributes', 'ims_theme' ),
      'parent_item_colon'     => __( 'Parent Item:', 'ims_theme' ),
      'all_items'             => __( 'All Jobs', 'ims_theme' ),
      'add_new_item'          => __( 'Add Job', 'ims_theme' ),
      'add_new'               => __( 'Add Job', 'ims_theme' ),
      'new_item'              => __( 'New Job', 'ims_theme' ),
      'edit_item'             => __( 'Edit Job', 'ims_theme' ),
      'update_item'           => __( 'Update Job', 'ims_theme' ),
      'view_item'             => __( 'View Job', 'ims_theme' ),
      'view_items'            => __( 'View Jobs', 'ims_theme' ),
      'search_items'          => __( 'Search Job', 'ims_theme' ),
      'not_found'             => __( 'Not found', 'ims_theme' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'ims_theme' ),
      'featured_image'        => __( 'Featured Image', 'ims_theme' ),
      'set_featured_image'    => __( 'Set featured image', 'ims_theme' ),
      'remove_featured_image' => __( 'Remove featured image', 'ims_theme' ),
      'use_featured_image'    => __( 'Use as featured image', 'ims_theme' ),
      'insert_into_item'      => __( 'Insert into Job', 'ims_theme' ),
      'uploaded_to_this_item' => __( 'Uploaded to this Job', 'ims_theme' ),
      'items_list'            => __( 'Jobs list', 'ims_theme' ),
      'items_list_navigation' => __( 'Jobs list navigation', 'ims_theme' ),
      'filter_items_list'     => __( 'Filter Jobs list', 'ims_theme' ),
    ],
    'supports'              => [ 'title', 'editor', 'custom-fields' ],
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-tickets',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => false,
    'can_export'            => false,
    'has_archive'           => false,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'rewrite'               => [
      'slug'                  => 'careers',
      'with_front'            => true,
      'pages'                 => true,
      'feeds'                 => true,
    ],
    'capability_type'       => 'post',
    'show_in_rest'          => true,
  ]);
?>
