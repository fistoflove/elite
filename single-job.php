<?php
use Timber\Timber;
use Timber\PostQuery;

$context = Timber::context();

if ( is_singular( 'job' ) ) {
    $context['post'] = Timber::get_post();

    $context['fields'] = get_fields();

    $context['jobs'] = Timber::get_posts( [
        'post_type' => 'job',
        'posts_per_page' => -1,
        'post__not_in' => [$context['post']->id],
    ]);
    Timber::render( 'single-job.twig', $context );
}