<?php
use Timber\Timber;
use Timber\PostQuery;

$context = Timber::get_context();

Timber::render( 'index.twig', $context );
